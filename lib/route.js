Router.configure({
  noRoutesTemplate: true
});

FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayoutWithoutLogin', { main: 'Home' });
    }
})

FlowRouter.route('/profile', {
    name: 'profile',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Profile' });
    }
})

FlowRouter.route('/members', {
    name: 'members',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Members' });
    }
})


FlowRouter.route('/pay', {
    name: 'members',
    action() {
        BlazeLayout.render('MainLayout', { main: 'HelloAsso' });
    }
})

FlowRouter.route('/profile/:mbId', {
    name: 'member',
    action: function(params, queryParams) {
        BlazeLayout.render('MainLayout', { main: 'Member' });
    }
})

FlowRouter.route('/stages', {
    name: 'internships',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Internships' });

    }
})

FlowRouter.route('/map', {
    name: 'map',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Map' });

    }
})

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Login' });
    }
})

FlowRouter.route('/psw', {
    name: 'psw',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Password' });
    }
})

FlowRouter.route('/reset-password/:token', {
    name: 'reset-psw',
    action(params, queryParams) {
        BlazeLayout.render('MainLayoutWithoutLogin', { main: 'Password' });
    }
})

FlowRouter.route('/signup', {
    name: 'sign-up',
    action() {
        BlazeLayout.render('MainLayoutWithoutLogin', { main: 'Register' });
    }
})

FlowRouter.route('/admin', {
    name: 'sign-up',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Admin' });
    }
})
