Internships = new Mongo.Collection("internships");

InternshipSchema = new SimpleSchema({
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert)
                return new Date;
            if (this.isUpsert)
                return { $setOnInsert: new Date() };
            if (this.isUpdate)
                this.unset();
            return
        },
        autoform: {
            type: 'hidden',
        },
        optional: true,
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            return new Date();
        },
        autoform: {
            type: 'hidden',
        },
        optional: true,
    },
    title: {
        type: String,
        label: "Titre"
    },
    enterprise: {
        type: String,
        label: "Nom de l'entreprise"
    },
    location: {
        type: String,
        label: "Position géographique"
    },
    begin: {
        type: Date,
        optional: true,
        label: "Début"

    },
    duration: {
        type: Number,
        optional: true,
        label: "Durée en semaine"
    },
    expiration: {
        type: Date,
        label: "Date d'expiration"
    },
    field: {
        type: String,
        optional: true,
        allowedValues: Meteor.settings.public.secteurs,
        label: "Secteur"

    },
    teasing: {
        type: String,
        label: "Accroche",
        autoform: {
            afFieldInput: {
                type: 'summernote',
                class: 'editor',
                settings: {
                    height: 100,
                    dialogsInBody: true,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                }
            }
        }
    },
    summary: {
        type: String,
        label: "Description",
        autoValue: function() {
            if (Meteor.isServer) {
                console.log(this.value, sanitizeHtml(this.value));
                return sanitizeHtml(this.value);
            } else {
                return this.value;
            }
        },
        autoform: {
            afFieldInput: {
                type: 'summernote',
                class: 'editor',
                settings: {
                    height: 100,
                    dialogsInBody: true,
                    //airMode: true,
                }
            }
        }
    },
    profile: {
        type: String,
        optional: true,
        label: "Profil",
        autoform: {
            afFieldInput: {
                type: 'summernote',
                class: 'editor',
                settings: {
                    height: 100,
                    dialogsInBody: true,
                }
            }
        }
    },
    author: {
        type: String,
        label: "Author",
        autoValue: function() {
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return { $setOnInsert: this.userId };
            } else {
                this.unset();
            }

        },
        autoform: {
            type: "hidden"
        }
    },
    validated: {
        type: Boolean,
        optional: true,
        autoValue: function() {
            if (this.isInsert)
                return false;
            if (this.isUpsert)
                return { $setOnInsert: new Date() };
            if (this.isUpdate)
                this.unset();
            return
        },
        autoform: {
            type: "hidden"
        }
    },
    archived: {
        type: Boolean,
        optional: true,
        autoValue: function() {
            if (this.isInsert)
                return false;
            if (this.isUpsert)
                return { $setOnInsert: new Date() };
            if (this.isUpdate)
                this.unset();
            return
        },
        autoform: {
            type: "hidden"
        }
    }
});

Internships.attachSchema(InternshipSchema);
