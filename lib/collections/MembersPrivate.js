MembersPrivate = new Mongo.Collection("membersprivate");

Schemas.MembersPrivate = new SimpleSchema({
    birth: {
        type: Date,
        label: "Date de Naissance",
        optional: true
    },
    mail: {
        type: String,
        label: "E-Mail",
        optional: true
    },
    phone: {
        type: String,
        label: "Téléphone",
        optional: true
    },
    facebook: {
        type: String,
        label: "Lien Facebook",
        optional: true
    },
    twitter: {
        type: String,
        label: "Lien Twitter",
        optional: true
    },
    linkedin: {
        type: String,
        label: "Lien Linkedin",
        optional: true
    },
    address: {
        type: String,
        label: "Adresse",
        optional: true
    },
    postalcode: {
        type: String,
        label: "Code postal",
        optional: true
    },
    city: {
        type: String,
        label: "Ville",
        optional: true
    },
    country: {
        type: String,
        label: "Pays",
        optional: true
    },
    author: {
        type: String,
        label: "Author",
        autoValue: function() {
            if (this.isInsert) {
                return this.userId
            } else if (this.isUpsert) {
                return { $setOnInsert: this.userId };
            } else {
                this.unset();
            }

        },
        autoform: {
            type: "hidden"
        }
    }
});

MembersPrivate.attachSchema(Schemas.MembersPrivate);


