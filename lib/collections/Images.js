Images = new Mongo.Collection('images');

ImageSchema = new SimpleSchema({
    imageurl: {
        type: String,
        label: "Image URL"
    },
    author: {
        type: String,
        label: "Author",
        autoValue: function() {
            if (this.isSet) {
                return this.value;
            }
            if (this.isInsert) {
                return this.userId
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: this.userId
                };
            } else {
                this.unset();
            }

        },
        autoform: {
            type: "hidden"
        }
    }
});

Images.attachSchema(ImageSchema);
