//require('./yourfile.js');

Schemas = {};

Members = new Mongo.Collection("members");

Schemas.Members = new SimpleSchema({
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert)
                return new Date;
            if (this.isUpsert)
                return {
                    $setOnInsert: new Date()
                };
            if (this.isUpdate)
                this.unset();
            return
        },
        autoform: {
            type: 'hidden',
        },
        optional: true,
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            return new Date();
        }
    },
    gender: {
        type: String,
        label: "Sexe",

    },
    firstname: {
        type: String,
        label: "Prénom",

    },
    name: {
        type: String,
        label: "Nom",

    },
    birth: {
        type: Date,
        label: "Date de Naissance",
        optional: true,

    },
    description: {
        type: String,
        label: "Description",
        optional: true,

    },
    association: {
        type: String,
        label: "Parcours associatif",
        optional: true,

    },
    international: {
        type: String,
        label: "Expériences à l'international",
        optional: true,

    },
    mail: {
        type: String,
        label: "E-Mail",
        optional: true,

    },
    phone: {
        type: String,
        label: "Téléphone",
        optional: true,

    },
    facebook: {
        type: String,
        label: "Lien Facebook",
        optional: true,

    },
    twitter: {
        type: String,
        label: "Lien Twitter",
        optional: true,

    },
    linkedin: {
        type: String,
        label: "Lien Linkedin",
        optional: true,

    },
    address: {
        type: String,
        label: "Adresse",
        optional: true,

    },
    postalcode: {
        type: String,
        label: "Code postal",
        optional: true,

    },
    city: {
        type: String,
        label: "Ville",
        optional: true,

    },
    country: {
        type: String,
        label: "Pays",
        optional: true,

    },
    status: {
        type: String,
        label: "Statut",
        optional: true,

    },
    diploma: {
        type: String,
        label: "Diplôme",
        optional: true
    },
    protitle: {
        type: String,
        label: "Titre",
        optional: true
    },
    profield: {
        type: String,
        label: "Secteur",
        optional: true
    },
    school: {
        type: String,
        label: "Lycée",
        optional: true
    },
    bac: {
        type: String,
        label: "Baccalauréat",
        optional: true
    },
    entrep: {
        type: Number,
        optional: true,
    },
    home: {
        type: Number,
        optional: true,

    },
    helper: {
        type: Boolean,
        optional: true,
    },
    author: {
        type: String,
        label: "Author",

        autoValue: function() {
            if (this.isSet) {
                return this.value;
            }
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: this.userId
                };
            } else {
                this.unset();
            }

        },
        autoform: {
            type: "hidden"
        }
    }
});

Members.attachSchema(Schemas.Members);
