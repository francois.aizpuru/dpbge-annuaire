import Tabular from 'meteor/aldeed:tabular';


TabularTables = {};

TabularTables.Users = new Tabular.Table({
    name: "Users",
    collection: Meteor.users,
    searching: false,
    allow(userId) {
        return userId; //&& Roles.userIsInRole(userId, 'admin');
    },
    columns: [
        { title: "Fiche", tmpl: Meteor.isClient && Template.detailscontact, class: "fit" }, {
            data: "emails",
            title: "Email",
            render: function(val, type, doc) {
                if(val){
                    return val[0].address;
                } return "";
            }
        },
        { title: "Validé", tmpl: Meteor.isClient && Template.validatedcontact, class: "fit" },
        { title: "Admin", tmpl: Meteor.isClient && Template.admincontact, class: "fit" },

    ]
});