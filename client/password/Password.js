Template.Password.onRendered(function() {
    Session.set('changeError', "");
});

Template.Password.events({
    'submit': function(event) {
        event.preventDefault();
        old = event.target.oldpassword ? event.target.oldpassword.value : '';
        newP = event.target.password.value;
        rep = event.target.passwordrepeat.value;
        if (newP != rep) {
            launchAnimation(document.getElementById("modifPasswBtn"), "shake-animation");
            Session.set('changeError', "Les deux mots de passe ne correspondent pas.");
        } else {
            let errorFunction = function(err) {
                if (err) {
                    launchAnimation(document.getElementById("modifPasswBtn"), "shake-animation");
                    Session.set('changeError', err.reason);
                } else {
                    FlowRouter.go('login');
                }
            }
            if (FlowRouter.getParam('token')) {
                Accounts.resetPassword(FlowRouter.getParam('token'), newP, errorFunction);
            } else {
                Accounts.changePassword(old, newP, errorFunction);
            }
        }
    }
});

Template.Password.helpers({
    'error': function() {
        return Session.get('changeError');
    },
    'hasToken': function() {
        console.log(FlowRouter.getParam('token'));
        return FlowRouter.getParam('token');

    }
});
