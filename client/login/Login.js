import mailchimpAPI from 'meteor/universe:mailchimp-v3-api';

if (Meteor.isClient) {
  Template.dashboard.events({
    'click .logout': function(event) {
      console.log("click");
      event.preventDefault();
      Meteor.logout();
    },
  });

  Template.registerPanel.events({
    'submit': function(event) {
      event.preventDefault();
      var email = event.target.email.value;
      var password = event.target.password.value;
      var passwordRepeat = event.target.passwordrepeat.value;
      if (password === passwordRepeat) {
        Accounts.createUser({
          email: email,
          password: password
        }, function(err) {
          if (err) {
            console.log(err);

          } else {
            Meteor.call('roles.init');
          }
        });
      } else {
        launchAnimation(document.getElementById("passwordrepeat"), "shake-animation");
        addClass(document.getElementById("passwordrepeat"), "has-error");
      }
    }
  });

  Template.loginPanel.events({
    'submit .loginform': function(event) {
      event.preventDefault();
      var emailVar = event.target.email.value;
      var passwordVar = event.target.password.value;
      Meteor.loginWithPassword(emailVar, passwordVar, function(err) {
        if (err) {
          launchAnimation(document.getElementById("login-block"), "shake-animation");
          Session.set('loginError', err.reason);
        }
      });
    }
  });

  Template.modalforget.events({
    'submit .recoverform': function(event) {
      event.preventDefault();
      Accounts.forgotPassword({
          email: event.target.recovermail.value
        },
        function(err) {
          console.log(err);
        });
    }
  });


  Template.loginPanel.helpers({
    loginError: function() {
      return Session.get('loginError');
    }
  });

}


function addClass(ele, cls) {
  if (!ele.classList.contains(cls)) {
    ele.classList.add(cls);
  }
}

launchAnimation = function(ele, cls) {
  if (ele.classList.contains(cls)) {
    var newone = ele.cloneNode(true);
    ele.parentNode.replaceChild(newone, ele);
  } else {
    ele.classList.add(cls);
  }
}