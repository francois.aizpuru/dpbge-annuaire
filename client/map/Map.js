import "leaflet";
import "leaflet.markercluster";


Template.Map.onCreated(function bodyOnCreated() {
    Meteor.subscribe('positions');
    Meteor.subscribe('members', () => Meteor.defer(() => setTimeout(leaflet, 200)));
    Meteor.subscribe('users');

});


function leaflet() {
    const maxZoom = 11;

    var buildAdress = entry => {
        return entry.city + ", " + entry.country;
    }
    var mymap = L.map('mapid', {
        center: [51.505, -0.09],
        maxZoom: maxZoom,
        zoom: 9
    });

    var membersToDisplay = Members.find().fetch()
        .filter(member => Roles.userIsInRole(member.author, ['admin', 'validated']));
    var membersSummary = membersToDisplay
        .map(member => {
            var position = Positions.findOne({
                address: buildAdress(member)
            });
            return {
                position: position && [position.lat, position.lng] || undefined,
                id: member._id
            }
        })
        .filter(el => el.position);

    mymap.fitBounds(membersSummary.map(member => member.position), {
        padding: [50, 50]
    });


    var markers = membersSummary.map(member => {
        var marker = L.marker(member.position, {});
        marker.bindPopup(memberRender(membersToDisplay.filter(mb => mb._id == member.id)[0]));
        return marker;
    });


    var cluster = L.markerClusterGroup({
        spiderfyOnMaxZoom: false
    });
    markers.forEach(marker => cluster.addLayer(marker));
    cluster.on('clusterclick', function (a) {
        if (a.layer._zoom == maxZoom) {
            var markersInCluster = a.layer.getAllChildMarkers();
            L.popup()
                .setLatLng(a.layer._latlng)
                .setContent(markersInCluster.length +
                    " membres:<br>" +
                    markersInCluster.map(child => child._popup._content)
                    .join('<br>'))
                .openOn(mymap);
        }
    });

    mymap.addLayer(cluster);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: maxZoom,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(mymap);
};

var memberRender = function (member) {
    value = "<a onClick='displayMember(\"" + member.author + "\");'>" + member.firstname + " " + member.name + '</a>';
    if (member.facebook) {
        value += " <a href=" + member.facebook + "><i class='fa fa-facebook-official'></i></a>";
    }
    if (member.twitter) {
        value += " <a href=" + member.twitter + "><i class='fa fa-twitter'></i></a>";
    }
    if (member.linkedin) {
        value += " <a href=" + member.linkedin + "><i class='fa fa-linkedin'></i></a>";
    }
    if (member.mail) {
        value += " <a href=mailto:" + member.mail + "><i class='fa fa-envelope'></i></a>";
    }
    if (member.phone) {
        value += " <a href=tel:" + member.phone + "><i class='fa fa-phone'></i></a>";
    }
    return value;
}