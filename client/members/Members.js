
var Blazy = require('blazy');
var bLazy = new Blazy();

Template.Members.onCreated(function bodyOnCreated() {
    Deps.autorun(function() {
        Meteor.subscribe('members', function() {
            bLazy = new Blazy();
        });
        Meteor.subscribe('images', function() {
            bLazy = new Blazy();
        });
        Meteor.subscribe('users', function() {
            bLazy = new Blazy();
        });
        Meteor.subscribe('positions', function() {
            bLazy = new Blazy();
        });
    });
});




cleanSelector = function() {
    Session.set('schoolSelector', "");
    Session.set('profieldSelector', "");
    Session.set('diplomaSelector', "");
    Session.set('positionSelector', "");
    Session.set('fromSelector', "");
    Session.set('toSelector', "");
}

unveil = function() {
    Meteor.defer(function() {
        bLazy = new Blazy();
        $("a").click(function(e) {
            e.stopPropagation();
        });
    });

}

Template.Members.onRendered(function() {
    Session.set('advSearch', false);
    Session.set('search', "");
    cleanSelector();


    this.autorun(function() {
        if (Members.find().count()) {
            Tracker.afterFlush(function() {
                unveil();
            });
        }
    });
});

displayMember = function(id) {
    Modal.show('modal', function() {
        return Members.findOne({
            author: id
        });
    });
};


var keys = ['firstname', 'name', 'mail', 'phone', 'address', 'postalcode', 'city', 'country', 'status', 'diploma', 'protitle', 'profield', 'school', 'description']

Template.Members.helpers({});

Template.memberItem.helpers({
    capitalize: function(string) {
        return capitalize(string);
    },
    relevantSchool: function() {
        return this.member.school && this.member.school != "Autre";
    }
});



Template.MembersPanel.helpers({
    advSearch: function() {
        return Session.get('advSearch');
    },
    lycees: function() {
        return Meteor.settings.public.lycees;
    },
    profields: function() {
        return Meteor.settings.public.secteurs;
    },
    citys: function() {
        return _.uniq(Positions.find().map(function(el) {
            return capitalize(el.address.split(",")[0])
        })).sort();
    },
    members: function() { //{"username" : /.*son.*/i}
        selector = {};
        if (Session.get('advSearch')) {
            school = Session.get('schoolSelector');
            profield = Session.get('profieldSelector');
            diploma = Session.get('diplomaSelector');
            position = Session.get('positionSelector');
            from = Session.get('fromSelector');
            to = Session.get('toSelector');

            selectors = [];
            if (school != "") {
                selectors.push({
                    school: school
                });
            }
            if (profield != "") {
                selectors.push({
                    profield: profield
                });
            }
            if (diploma != "") {
                selectors.push({
                    "diploma": {
                        '$regex': diploma,
                        '$options': 'i'
                    }
                });
            }
            if (position != "") {
                selectors.push({
                    "city": {
                        '$regex': position,
                        '$options': 'i'
                    }
                });
            }
            if (from != "") {
                selectors.push({
                    "birth": {
                        "$gte": moment(from, "YYYY").toDate()
                    }
                });
            }
            if (to != "") {
                selectors.push({
                    "birth": {
                        "$lte": moment(to, "YYYY").endOf('year').toDate()
                    }
                });
            }
            if (selectors.length == 1) {
                selector = selectors[0];
            } else if (selectors.length != 0) {
                selector = {
                    $and: selectors
                };
            }
        }
        return Members.find(selector, {
            sort: {
                name: 1,
                firstname: 1
            }
        });
    },
    isValidated: function(id) {
        return Roles.userIsInRole(id, ['admin', 'validated']);
    },
    search: function(doc) {
        var search = Session.get('search');
        var flatdoc = "";
        if (search) {
            for (index = 0; index < keys.length; ++index) {
                if (doc[keys[index]]) {
                    flatdoc += " " + Diacritics.remove(doc[keys[index]]);
                }
            }
            var searchvalues = Diacritics.remove(search).split(" ");
            var match = [];
            for (index2 = 0; index2 < searchvalues.length; ++index2) {
                if (!flatdoc.match(new RegExp(searchvalues[index2], 'i'))) {
                    return false;
                }
            }
        }
        return true;
    }
});



Template.MembersPanel.events({
    'input .search-box': function(event, template) {
        Session.set('search', event.target.value);
        unveil();
    },
    'change #profield': function(event) {
        Session.set('profieldSelector', event.target.value);
        unveil();
    },
    'change #school': function(event) {
        Session.set('schoolSelector', event.target.value);
        unveil();
    },
    'change .input-daterange': function(event) {
        Session.set(event.target.name + 'Selector', event.target.value);
        unveil();
    },
    'click #advSearch': function(event) {
        Session.set('advSearch', !Session.get('advSearch'));
        unveil();
        cleanSelector();
        Meteor.defer(function() {
            $(".input-daterange").datepicker({
                minViewMode: "years",
                format: "yyyy",
            });

        });

    },
    'input #position': function(event) {
        Session.set('positionSelector', event.target.value);
        unveil();
    },
    'input #diploma': function(event) {
        Session.set('diplomaSelector', event.target.value);
        unveil();
    }

});
