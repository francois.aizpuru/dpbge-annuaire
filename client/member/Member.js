Template.memberinfo.onRendered(function() {
    if (FlowRouter.getParam('mbId')) {
        Meteor.subscribe("members", Members.findOne({ author: FlowRouter.getParam('mbId') })._id);
    } else {
        Meteor.subscribe("members", this._id);
    }
});
Template.memberinfo.helpers({
    member: function() {
        if (FlowRouter.getParam('mbId')) {
            return Members.findOne({ author: FlowRouter.getParam('mbId') });
        }
        return this;
    },
    notempty: function(str, str2) {
        return str != undefined || str2 != undefined;
    }
})

Template.memberinfo.onCreated(function bodyOnCreated() {
    Meteor.subscribe('members', this._id);
    Meteor.subscribe('images', this._id);
});
