var uploader = new ReactiveVar();
var currentUserId = Meteor.userId();

Template.modalChangePicture.onRendered(function() {
  Meteor.defer(function() {
    $(".photo-in:contains('Drop')").addClass("btn btn-primary")
      .text("Ouvrir une image").css('width', '100%');
    $(".crop").addClass("btn btn-primary");
  });
  uploader.set();
});

Template.modalChangePicture.helpers({
  isUploading: function() {
    return Boolean(uploader.get());
  },

  progress: function() {
    var upload = uploader.get();
    if (upload)
      return Math.round(upload.progress() * 100);
  },
  photoUpOptions: function() {
    return {
      loadImage: {
        maxHeight: 300,
      },
      crop: true,
      jCrop: {
        aspectRatio: 1,
        minSize: [200, 200]
      },
      autoSelectOnJcrop: true,
      requiredAspectRatio: 1,
      callback: function(error, photo) {
        var upload = new Slingshot.Upload("myImageUploads");
        if (photo) {
          Meteor.defer(function() {
            $(".photo-up .card .card-image .card-tagline").css('visibility', 'hidden');
            $("a.crop").addClass("btn btn-primary").css('width', '100%');
            $("a.reset").css('visibility', 'hidden');
          });

          crop = $(".crop");
          if (crop.length == 1) {
            uploader.set(upload);
            processImage(PhotoUp.DataURItoBlob(photo.src), 600, 600, function(data) {
              upload.send(PhotoUp.DataURItoBlob(data), function(error, downloadUrl) {
                if (error) {
                  console.error('Error uploading');
                  alert(error);
                } else {
                  console.log("Success!");
                  console.log('uploaded file available here: ' + downloadUrl);
                  Meteor.call('images.insert', downloadUrl);
                  $('.modal').modal('toggle');
                }
              });
            });

          }

        }
      }
    }
  },
});



Template.Profile.onRendered(function() {

  var self = this;
  self.autorun(function() {
    self.subscribe("members", Meteor.userId(), function() {
      var current = Members.findOne({
        author: Meteor.userId()
      });
      if (current) {
        $("[name='gender" + current.gender + "']").prop('selected', true);
        $("[name='status" + current.status + "']").prop('selected', true);
        $("[name='profield" + current.profield + "']").prop('selected', true);
        $("[name='school" + current.school + "']").prop('selected', true);
        $("[name='bac" + current.bac + "']").prop('selected', true);

        if (current.mail) {
          Meteor.call('mailchimp.status', current.mail, function(error, result) {
            if (result && result.data.status != "subscribed") {
              $("#newsletter").attr('checked', false);
            }
          });
        }
      }



      $("#slider-entrep").noUiSlider({
        start: current && current.entrep ? current.entrep : -1,
        animate: true,
        animationDuration: 300,
        range: {
          'min': -1,
          'max': 4
        }
      }).on('slide', function(ev, val) {
        Session.set('sliderentrep', val);
      });
      Session.set('sliderentrep', current && current.entrep ? current.entrep : -1);



      $("#slider-home").noUiSlider({
        start: current && current.home ? current.home : -1,
        range: {
          'min': -1,
          'max': 4
        }
      }).on('slide', function(ev, val) {
        Session.set('sliderhome', val);
      });
      Session.set('sliderhome', current && current.home ? current.home : -1);

    });
    self.subscribe('images', Meteor.userId());
    self.subscribe('positions');
    self.subscribe('membersPrivate', Meteor.userId(), function() {
      var currentPrivate = MembersPrivate.findOne({
        author: Meteor.userId()
      });
      for (var index in currentPrivate) {
        if (currentPrivate[index] && index != "_id" && index != "author") {
          i = $("[name='" + index + "visi" + "']").find('i');
          i.removeClass("ion-eye");
          i.addClass("ion-eye-disabled");
        }
      }
    });
  });

});

calculateEntrepValue = function(value) {
  switch (parseInt(value)) {
    case -1:
      return "Ne se prononce pas";
      break;
    case 0:
      return "Pas du tout intéréssé(e)";
      break;
    case 1:
      return "Peu intéréssé(e)";
      break;
    case 2:
      return "Neutre";
      break;
    case 3:
      return "Intéréssé(e)";
      break;
    case 4:
      return "Tout à fait intéréssé(e)";
      break;
    default:
      return "";
  }
}
Template.registerHelper('entrep', (value) => {
  return calculateEntrepValue(value);
});

calculateBackHomeValue = function(value) {
  switch (parseInt(value)) {
    case -1:
      return "Ne se prononce pas";
      break;
    case 0:
      return "Situation actuelle";
      break;
    case 1:
      return "Moins de 3 ans";
      break;
    case 2:
      return "Moins de 10 ans";
      break;
    case 3:
      return "La retraite";
      break;
    case 4:
      return "Je n'y pense pas";
      break;
    default:
      return "";
  }
}
Template.registerHelper('backHome', (value) => {
  return calculateBackHomeValue(value);
});

Template.Profile.helpers({
  entrepslider: function() {
    return calculateEntrepValue(Session.get('sliderentrep'));
  },
  homeslider: function() {
    return calculateBackHomeValue(Session.get('sliderhome'));
  },
  selected: function(field, value) {
    current = Members.findOne({
      author: Meteor.userId()
    });
    if (!current) {
      return value == '';
    } else {
      //console.log(current[field], value, current[field] == value);
      return current[field] == value;
    }
  },
  get: function(field) {
    mbf = null;
    mbpf = null;
    mb = Members.findOne({
      author: Meteor.userId()
    });
    if (field == "helper") {
      return mb && mb.helper;
    }
    if (mb) {
      mbf = mb[field];
    }
    mbp = MembersPrivate.findOne({
      author: Meteor.userId()
    });
    if (mbp) {
      mbpf = mbp[field];
    }
    if (mbf) {
      return mbf;
    } else if (mbpf) {
      return mbpf;
    } else {
      return "";
    }
  },
  currentmember: function() {
    var current = Members.findOne({
      author: Meteor.userId()
    });
    if (current == null) {
      return {};
    } else {
      return current;
    }
  },
  privatemember: function() {
    var pvcurrent = MembersPrivate.findOne({
      author: Meteor.userId()
    });
    if (pvcurrent == null) {
      return {};
    } else {
      return pvcurrent;
    }
  },
  lycees: function() {
    return Meteor.settings.public.lycees;
  },
  bacs: function() {
    return Meteor.settings.public.bacs;
  },
  secteurs: function() {
    return Meteor.settings.public.secteurs;
  },
  statuses: function() {
    return Meteor.settings.public.statuses;
  },
});

function buildAddress(entry) {
  return entry.city.value + ", " + entry.country.value;
}


addLocation = function(doc) {
  address = buildAddress(doc);
  if (!Positions.findOne({
      address: address
    })) {
    var geocoder = new google.maps.Geocoder();
    if (address.indexOf("undefined") == -1) {
      geocoder.geocode({
        'address': address
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          Meteor.call('positions.insert', {
            address: address,
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          });
        } else {
          console.log("Geocode was not successful for the following reason: " + status);
        }

      });
    }
  }
}

capitalize = function(string) {
  arrayString = string.split(" ");
  for (var i = 0; i < arrayString.length; i++) {
    arrayString[i] = arrayString[i].charAt(0).toUpperCase() + arrayString[i].slice(1).toLowerCase();
  }
  return arrayString.join(" ");

}

addMailChimp = function(doc) {
  Meteor.call('mailchimp.manage', doc);
}

var linkedinImport = function() { // Setup an event listener to make an API call once auth is complete
  IN.UI.Authorize().params({
    "scope": ["r_basicprofile", "r_emailaddress"]
  }).place();
  IN.Event.on(IN, 'auth', importLinkedinProfile);
}

var importLinkedinProfile = function() { // Use the API call wrapper to request the member's basic profile data
  var askedFields = [
    "id",
    "firstName",
    "lastName",
    "email-address",
    "headline",
    "summary",
    "specialties",
    "picture-url",
    "picture-urls::(original)",
    "public-profile-url",
    "location",
    "industry",
    "positions"
  ];
  IN.API.Profile("me")
    .fields(askedFields.join(','))
    .result(function(me) {
      var profile = me.values[0];
      Meteor.call('images.insert', profile['pictureUrl']);
      setInput('firstname', profile['firstName']);
      setInput('name', profile['lastName']);
      setInput('linkedin', profile['publicProfileUrl']);
      setInput('protitle', profile['headline']);
      setInput('description', profile['summary']);
      setInput('mail', profile['email-adress']);
      //setInput('city', profile.location.split('area').join(' '));
      //setInput('country', profile.location.country)
      //console.log(profile.location.country.code);
      //console.log(countrynames.getName(profile.location.country.code));

      console.log(profile);
    });
}

var setInput = function(inputName, value) {
  if (value) {
    document.getElementsByName(inputName)[0].value = value;
  }
}

Template.Profile.events({
  'click .linkedin': function(event) {
    event.preventDefault();
    linkedinImport();
  },
  'click .changepicture': function(event) {
    event.preventDefault();
    Modal.show("modalChangePicture");
  },

  'submit .update-profile-form': function(event) {
    event.preventDefault();
    var member = {
      gender: {
        value: event.target.gender.value,
        visible: true
      },
      firstname: {
        value: event.target.firstname.value,
        visible: true
      },
      name: {
        value: event.target.name.value,
        visible: true
      },
      birth: {
        value: parseDate(event.target.birth.value),
        visible: $(event.target.birthvisi).find('i').hasClass('ion-eye')
      },
      description: {
        value: event.target.description.value,
        visible: true
      },
      association: {
        value: event.target.association.value,
        visible: true
      },
      international: {
        value: event.target.international.value,
        visible: true
      },
      mail: {
        value: event.target.mail.value,
        visible: $(event.target.mailvisi).find('i').hasClass('ion-eye')
      },
      phone: {
        value: event.target.phone.value,
        visible: $(event.target.phonevisi).find('i').hasClass('ion-eye')
      },
      facebook: {
        value: event.target.facebook.value,
        visible: $(event.target.facebookvisi).find('i').hasClass('ion-eye')
      },
      twitter: {
        value: event.target.twitter.value,
        visible: $(event.target.twittervisi).find('i').hasClass('ion-eye')
      },
      linkedin: {
        value: event.target.linkedin.value,
        visible: $(event.target.linkedinvisi).find('i').hasClass('ion-eye')
      },
      address: {
        value: event.target.address.value,
        visible: false
      },
      postalcode: {
        value: event.target.postalcode.value,
        visible: true
      },
      city: {
        value: capitalize(event.target.city.value),
        visible: true
      },
      country: {
        value: capitalize(event.target.country.value),
        visible: true
      },
      status: {
        value: event.target.status.value,
        visible: true
      },
      diploma: {
        value: event.target.diploma.value,
        visible: true
      },
      protitle: {
        value: event.target.protitle.value,
        visible: true
      },
      profield: {
        value: event.target.profield.value,
        visible: true
      },
      school: {
        value: event.target.school.value,
        visible: true
      },
      bac: {
        value: event.target.bac.value,
        visible: true
      },
      home: {
        value: parseInt(Session.get('sliderhome')),
        visible: true
      },
      entrep: {
        value: parseInt(Session.get('sliderentrep')),
        visible: true
      },
      helper: {
        value: event.target.helper.checked,
        visible: true
      },
    };
    var mb = Object.keys(member).reduce(function(previous, current) {
      previous[current] = member[current].value;
      return previous;
    }, {});
    Meteor.call('members.upsert', member);
    addLocation(event.target);
    doc = {
      "email_address": event.target.mail.value,
      "status": event.target.newsletter.checked ? "subscribed" : "unsubscribed",
      "merge_fields": {
        "FNAME": event.target.firstname.value,
        "LNAME": event.target.name.value,
        "LYCEE": event.target.school.value,
        "STATUT": event.target.status.value,
        "SECTEUR": event.target.profield.value,
        "GENRE": event.target.gender.value,
        "NAISSANCE": event.target.birth.value,
        "PARRAIN": event.target.helper.value
      }
    };
    addMailChimp(doc);
    Modal.show("modalEditSaved");
  },
  'click .visibility': function(event) {
    event.preventDefault();
    i = $(event.target).find('i');
    if (i.attr('class') == undefined) {
      i = $(event.target).closest('i');
    }
    if (i.hasClass("ion-eye")) {
      i.removeClass("ion-eye");
      i.addClass("ion-eye-disabled");
    } else if (i.hasClass("ion-eye-disabled")) {
      i.removeClass("ion-eye-disabled");
      i.addClass("ion-eye");
    }
  },
});


function parseDate(stringdate) {
  if (stringdate) {
    return new Date(stringdate);
  } else {
    return null;
  }
}