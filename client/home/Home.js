Template.homePanel.helpers({
  assoName: function(){
    return Meteor.settings.public.asso.name;
  },
  assoShortName: function(){
    return Meteor.settings.public.asso.shortname;
  }
});
