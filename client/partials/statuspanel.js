Template.statusPanel.helpers({
    profileNotCreated: function() {
        return (!Members.findOne({ author: Meteor.userId() }));
    },
    userNotValidated: function() {
        return !Roles.userIsInRole(Meteor.userId(), 'validated');
    }
});
