Template.footer.helpers({
    currentYear: function() {
        return moment().format('YYYY');
    },
    developedBy: function() {
        return Session.get('credit');
    },
    assoFacebook: function() {
      return Meteor.settings.public.asso.fb;
    },
    assoTwitter: function() {
      return Meteor.settings.public.asso.twitter;
    },
    assoLinkedin: function() {
      return Meteor.settings.public.asso.linkedin;
    },
    assoNewletter: function() {
      return Meteor.settings.public.asso.newsletter;
    }
});

Template.footer.events({
    'click .copyright': function() {
        Session.set('credit', 'Developed with <div style="color:#c0392b; display:inline;">♥</div>  by François AIZPURU');
    }
});
