Template.Admin.onCreated(function bodyOnCreated() {
    Meteor.subscribe('members');
    Meteor.subscribe('membersPrivate');
    Meteor.subscribe('images');
    Meteor.subscribe('users');

    Session.set('selectorChoice', null);
});

displayMember = function(id) {
    Modal.show('modal', function() {
        return Members.findOne({ author: id });
    });
};


Template.Admin.helpers({
    selector: function() {
        if (Session.get('selectorChoice')) {
            switch (Session.get('selectorChoice')) {
                case "Tous":
                    return {};
                    break;
                case "A valider":
                    return { 'roles': { $nin: ['validated', 'admin'] } };
                    break;
                case "Admin":
                    return { 'roles': { $in: ['admin'] } };
                    break;
            }
        }
        return {};
    }
});
Template.detailscontact.helpers({

    waitingForInfo: function() {
        return !Members.findOne({ author: this._id });
    },
    ident: function() {
        mb = Members.findOne({ author: this._id });
        return mb.firstname + " " + mb.name;
    },
});

Template.admincontact.helpers({
    isAdmin: function() {
        if (Roles.userIsInRole(this._id, 'admin')) {
            return "checked";
        }
    },
    disabledIfMe: function() {
        return Meteor.userId() == this._id ? "disabled" : "";
    },
});

Template.validatedcontact.helpers({
    isValidated: function() {
        if (Roles.userIsInRole(this._id, ['admin', 'validated'])) {
            return "checked";
        }
    },
    disabledIfMe: function() {
        return Meteor.userId() == this._id ? "disabled" : "";
    },
});

Template.Admin.events({
    'click .admin-toggle': function(event, template) {
        Meteor.call(event.target.checked ? 'roles.add' : 'roles.remove', {
            id: this._id,
            roles: ['admin']
        });
    },
    'click .validated-toggle': function(event, template) {
        Meteor.call(event.target.checked ? 'roles.add' : 'roles.remove', {
            id: this._id,
            roles: ['validated']
        });
    },
    'change #selectorChooser': function(event, template) {
        Session.set('selectorChoice', event.target.value);
    },
    'click #exportExcel': function() {
        cleanObject = function(obj) {
            for (var propName in obj) {
                if (obj[propName] === null || obj[propName] === undefined) {
                    delete obj[propName];
                }
            }
            return obj;
        };
        formatDate = function(date) {
            if (date) {
                return moment(date).format("DD/MM/YYYY");
            }
            return "";
        }
        toExport = Members.find().map(function(el) {
            toreturn = Object.assign(el, cleanObject(MembersPrivate.findOne({ author: el.author })));
            toreturn.birth = formatDate(toreturn.birth);
            toreturn.createdAt = formatDate(toreturn.createdAt);
            toreturn.updatedAt = formatDate(toreturn.updatedAt);
            toreturn.entrep = calculateEntrepValue(toreturn.entrep);
            toreturn.home = calculateBackHomeValue(toreturn.home);
            return toreturn;
        });



        var csvContent = CSV.unparse(toExport);
        var encoded = 'data:text/csv;charset=latin1,' + encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encoded);
        link.setAttribute("download", "export.csv");
        document.body.appendChild(link); // Required for FF
        link.click();
    },
});
