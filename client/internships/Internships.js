Template.internshipPanel.helpers({
    currentinternships: function() {
        return Internships.find({ archived: false, validated: true });
    },
    unvalidatedinternships: function() {
        return Internships.find({ validated: false, archived: false });
    },
    archivedinternships: function() {
        return Internships.find({ archived: true });
    },
    currentinternshipscount() {
        return Internships.find({ archived: false, validated: true }).count();
    },
    unvalidatedinternshipscount: function() {
        return Internships.find({ validated: false, archived: false }).count();
    },
    archivedinternshipscount: function() {
        return Internships.find({ archived: true }).count();
    },
    noInternship: function() {
        return Internships.find().count() == 0;
    },
});

Template.Internships.onCreated(function() {
    Meteor.subscribe("members");
});

Template.modalInternship.helpers({
    modifMode: function() {
        if (this.internship) {
            return true;
        }
        return false;
    },
    doc: function() {
        return Internships.findOne(this.internship._id);
    }
});

Template.ModalArchiveInternship.events({
    'click .btn-success' (event) {
        event.preventDefault();
        Meteor.call('internships.set.archived', this.internship._id, !this.internship.archived);
    },
});

Template.ModalValidationInternship.events({
    'click .btn-success' (event) {
        event.preventDefault();
        Meteor.call('internships.set.validated', this.internship._id, !this.internship.validated);
    },
});

Template.ModalValidationInternship.helpers({
    action: function() {
        return (this.internship.validated ? "Révoquer " : "Valider ");
    },
});

Template.ModalArchiveInternship.helpers({
    action: function() {
        return (this.internship.archived ? "Restaurer " : "Archiver ");
    },
});

Template.internshipItem.helpers({
    ismyfield: function(field) {
        mb = Members.findOne({ author: Meteor.userId() });
        return (mb && mb.profield && mb.profield == field);
    },
    allowModifs: function(author) {
        return (author == Meteor.userId() || Roles.userIsInRole(Meteor.userId(), ['admin']));
    },
    isValidated: function(internship) {
        return internship.validated;
    },
    isArchived: function(internship) {
        return internship.archived;
    },
});

Template.modalDisplayInternship.helpers({
    author: function(){
        author =Members.findOne({author: this.internship
.author});
        return "<a target='_blank' href='/profile/"+this.internship.author+"'>"+author.firstname + " " + author.name+"</a>";
    }
});

Template.internshipItem.events({
    'click .valid' (event) {
        event.preventDefault();
        Modal.show('ModalValidationInternship', { internship: this.internship });
    },
    'click .archive' (event) {
        event.preventDefault();
        Modal.show('ModalArchiveInternship', { internship: this.internship });
    },
    'click .modif' (event) {
        event.preventDefault();
        Modal.show('modalInternship', { internship: this.internship });
    },
    'click .display-button' (event) {
        event.preventDefault();
        Modal.show('modalDisplayInternship', { internship: this.internship });
    }
});
