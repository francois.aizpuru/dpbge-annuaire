module.exports = {
  servers: {
    one: {
      host: 'dpbge.aizpuru.eu',
      username: 'root',
      // pem:
       password: '39w94STf'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    setupMongo: true,
    dockerImage: 'abernix/meteord:base',

  	// WARNING: Node.js is required! Only skip if you already have 		Node.js installed on server.
    setupNode: true,

  	// WARNING: nodeVersion defaults to 0.10.36 if omitted. Do not use v, just the version number.
    nodeVersion: "0.10.36",

  	// Install PhantomJS on the server
    setupPhantom: true,

  	// Show a progress bar during the upload of the bundle to the server.
  	// Might cause an error in some rare cases if set to true, for instance in Shippable CI
    enableUploadProgressBar: true,
    name: 'annuaire',
    path: '../',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://dpbge.aizpuru.eu',
      MONGO_URL: 'mongodb://localhost/meteor',
      MAIL_URL: 'smtp://AKIAIQCXKGXS7LKKOHCQ:AsPYE6eEf/m8hI5C0ybazDZMm9jNWYHmvxgGbZUtTmqT@email-smtp.eu-west-1.amazonaws.com:465'
    },

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },
  mongo: {
        oplog: true,
        port: 27017,
        servers: {
            one: {}
        }
    }
};
