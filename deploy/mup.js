module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: 'server.aizpuru.eu',
      username: 'root',
      // pem: './path/to/pem'
      // password: 'server-password'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
    // TODO: change app name and path
    name: 'annuaire',
    path: '../',


    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'https://server.aizpuru.eu',
      MONGO_URL: 'mongodb://localhost/meteor',
      MAIL_URL: 'smtp://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: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@smtp.sendgrid.net:587'
    },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      image: 'abernix/meteord:base',
      // imagePort: 80, // (default: 80, some images EXPOSE different ports)
    },

    // This is the maximum time in seconds it will wait
    // for your app to start
    // Add 30 seconds if the server has 512mb of ram
    // And 30 more if you have binary npm dependencies.
    deployCheckWaitTime: 60,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {}
    }
  },
  proxy: {
    domains: 'server.aizpuru.eu,www.server.aizpuru.eu',
    ssl: {
      // Enable let's encrypt to create free certificates
      letsEncryptEmail: 'francis.aizpuru@gmail.com',
      forceSSL: true
    }
  }
};