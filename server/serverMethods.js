import {
    Email
} from 'meteor/email'
import {
    MailChimp
} from './lib/mailchimp.js'


Meteor.methods({
    'roles.init': function() {
        if (Meteor.users.find().count() == 1) {
            Roles.addUsersToRoles(Meteor.userId(), ['admin']);

        }
    },
    'roles.add': function(doc) {
        if (Roles.userIsInRole(Meteor.userId(), ['admin'])) {
            Roles.addUsersToRoles(doc.id, doc.roles);
        }
    },
    'roles.remove': function(doc) {
        if (Roles.userIsInRole(Meteor.userId(), ['admin'])) {
            Roles.removeUsersFromRoles(doc.id, doc.roles);
        }
    },
    'mailchimp.status': function(mail) {
        return MailChimp.getStatus(mail);
    },
    'mailchimp.manage': function(doc) {
        return MailChimp.manage(doc);
    },
    'createFakeAccount': function(nb) {
        Fake.createFakeAccount(nb)
    },
    'images.insert': function(url) {
        if (Meteor.userId()) {
            Images.upsert({
                author: Meteor.userId()
            }, {
                $set: {
                    imageurl: url,
                }
            });
        }
    },
    'positions.insert': function(doc) {
        if (this.userId) {
            if (!Positions.findOne({
                    address: doc.address
                })) {
                Positions.insert(doc);
            }
        }
    },
    'members.upsert': function(doc) {
        var mb = Object.keys(doc).reduce(function(previous, current) {
            previous[current] = doc[current].visible ? doc[current].value : null;
            return previous;
        }, {});

        var mbprivate = Object.keys(doc).reduce(function(previous, current) {
            previous[current] = doc[current].visible ? null : doc[current].value;
            return previous;
        }, {});


        if (Meteor.userId()) {
            Members.upsert({
                    author: Meteor.userId()
                }, {
                    $set: mb
                }

            );
            if (Object.keys(mbprivate).length != 0) {
                MembersPrivate.upsert({
                        author: Meteor.userId()
                    }, {
                        $set: mbprivate
                    }

                );
            }

            /*if (Meteor.isServer && !Roles.userIsInRole(Meteor.userId(), ['validated', 'admin'])) {
                Email.send({
                    to: "francois.aizpuru@gmail.com",
                    from: "contact@dpbge.aizpuru.eu",
                    subject: "Validation en attente",
                    text: doc.firstname + " " + doc.name + " attend la validation de son profil."
                });
            }*/
        }
    },
    'send.mail' () {
        Email.send({
            to: "francois.aizpuru@gmail.com",
            from: "Intranet DPBGE <contact@dpbge.aizpuru.eu>",
            subject: "Test du mail",
            text: "Corps du mail"
        });
    },
    'internships.set.validated' (id, valid) {
        check(id, String);
        check(valid, Boolean);


        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        } else {
            Internships.update(id, {
                $set: {
                    validated: valid
                }
            }, {
                getAutoValues: false
            });
        }
    },
    'internships.set.archived' (id, archived) {
        check(id, String);
        check(archived, Boolean);

        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        } else {
            Internships.update(id, {
                $set: {
                    archived: archived
                }
            }, {
                getAutoValues: false
            });
        }
    }
});
