import {
  Meteor
} from 'meteor/meteor';
import {
  ServerConfig
} from './lib/serverConfig.js'
import {
  MailServer
} from './lib/mailServer.js'
import {
  MailChimp
} from './lib/mailchimp.js'
import {
  SlingshotParams
} from './lib/slingshotParams.js'
import {
  Linkedin
} from './lib/linkedin.js'


Meteor.startup(() => {
  MailServer.init();
  MailChimp.init();
  ServerConfig.init();
  SlingshotParams.init();
  Linkedin.init();
});