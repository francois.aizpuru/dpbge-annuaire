var ServerConfig = {
  init: function() {
    // Enable cross origin requests for all endpoints
    JsonRoutes.setResponseHeaders({
      "Cache-Control": "no-store",
      "Pragma": "no-cache",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET",
      "Access-Control-Allow-Headers": "Content-Type, Authorization, X-Requested-With"
    });


    Members._ensureIndex({
      "nom": 1,
      "prenom": 1
    });

    var roles = ['user', 'member', 'admin']
    if (Meteor.roles.find().count() === 0) {
      roles.map(function(role) {
        console.log(Roles.createRole(role));
      })
    }
  }
}

export {
  ServerConfig
};