var SlingshotParams = {
  init: function() {
    Slingshot.fileRestrictions("myImageUploads", {
      allowedFileTypes: ["image/png", "image/jpeg"],
      maxSize: 2 * 1024 * 1024,
    });

    Slingshot.createDirective("myImageUploads", Slingshot.S3Storage, {
      AWSAccessKeyId: Meteor.settings.private.AWS.AccessKeyID,
      AWSSecretAccessKey: Meteor.settings.private.AWS.SecretKey,
      bucket: Meteor.settings.private.AWS.bucket,
      acl: Meteor.settings.private.AWS.acl,
      region: Meteor.settings.private.AWS.region,

      authorize: function() {
        if (!this.userId) {
          var message = "Please login before posting images";
          throw new Meteor.Error("Login Required", message);
        }

        return true;
      },

      key: function(file) {
        return this.userId + "/profile" + moment().format("x") + ".jpg";
      }

    });
  }
}

export {
  SlingshotParams
};