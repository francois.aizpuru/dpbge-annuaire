import mailchimpAPI from 'meteor/universe:mailchimp-v3-api';
var MailChimp = {

  init: function() {
    mailchimpAPI.setApiKey(Meteor.settings.private.MailChimp.APIKey);
    /*mailchimpAPI._apiCall('POST', {
        resource: [
            {
                name: 'lists',
                value: Meteor.settings.private.MailChimp.listid
            },
            {
              name: 'merge_fields',
              value: ''
            }


        ],
        body: {
            "name": "TEST",
            "type": "text"
        }
    });*/
  },
  getStatus: function(mail) {
    return mailchimpAPI._apiCall('GET', {
      resource: [{
          name: 'lists',
          value: Meteor.settings.private.MailChimp.listid
        },
        {
          name: 'members',
          value: CryptoJS.MD5(mail).toString()
        }
      ]
    });
  },
  manage: function(doc) {
    mailchimpAPI._apiCall('GET', {
        resource: [{
            name: 'lists',
            value: Meteor.settings.private.MailChimp.listid
          },
          {
            name: 'members',
            value: CryptoJS.MD5(doc.email_address).toString()
          }
        ]
      })
      .catch(function(error) {
        if (error.response.statusCode == 404) {
          mailchimpAPI.addANewListMember({
            list_id: Meteor.settings.private.MailChimp.listid,
            body: doc
          });
        }
      })
      .then(function(value) {
        if (value.statusCode == 200) {
          if (value.data.merge_fields != doc.merge_fields) {
            mailchimpAPI._apiCall('PATCH', {
              resource: [{
                  name: 'lists',
                  value: Meteor.settings.private.MailChimp.listid
                },
                {
                  name: 'members',
                  value: CryptoJS.MD5(doc.email_address).toString()
                }
              ],
              body: doc,
            });
          }

        }
      });
  }
}

export {
  MailChimp
};