var Fake = {
  createFakeAccount: function(nb) {
    var faker = require('faker');
    faker.locale = "fr";
    for (var i = 0; i < nb || 0; i++) {
      let fakeMember = {
        gender: faker.random.arrayElement(['Homme', 'Femme']),
        firstname: faker.name.firstName(),
        name: faker.name.lastName(),
        birth: faker.date.past(),
        description: faker.lorem.paragraph(),
        mail: faker.internet.email(),
        phone: faker.phone.phoneNumber(),
        facebook: "facebook",
        twitter: "twitter",
        linkedin: "linkedin",
        address: faker.address.streetAddress(),
        postalcode: faker.address.zipCode(),
        city: faker.address.city(),
        country: faker.address.country(),
        status: faker.random.arrayElement(Meteor.settings.public.statuses),
        diploma: "",
        protitle: faker.name.jobTitle(),
        profield: faker.random.arrayElement(Meteor.settings.public.secteurs),
        school: faker.random.arrayElement(Meteor.settings.public.lycees),
        entrep: faker.random.number({
          min: -1,
          max: 4
        }),
        home: faker.random.number({
          min: -1,
          max: 4
        }),
        helper: faker.random.boolean()
      };

      fakeImage = {
        imageurl: faker.image.avatar()
      };
      let id = Accounts.createUser({
        email: fakeMember.mail,
        password: 'password'
      });
      fakeMember.author = id;
      fakeImage.author = id;
      Members.insert(fakeMember);
      Images.insert(fakeImage);
      Roles.addUsersToRoles(id, ['validated']);

    }
  }
}

export {
  Fake
};