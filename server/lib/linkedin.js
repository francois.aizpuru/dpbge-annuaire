var Linkedin = {
  "init": function() {
    ServiceConfiguration.configurations.update({
      "service": "linkedin"
    }, {
      $set: {
        "clientId": Meteor.settings.private.linkedin.clientId,
        "secret": Meteor.settings.private.linkedin.secret
      }
    }, {
      upsert: true
    });
  }
};

export {
  Linkedin
};