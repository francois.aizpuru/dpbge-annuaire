var MailServer = {
  init: function() {
    //init in deploy
    Accounts.emailTemplates.resetPassword.text = function(user, url) {
      url = url.replace('#/', '')
      return " Pour réinitialiser votre mot de passe, utililsez le lien suivant:\n\n" +
        url;
    };
    Accounts.emailTemplates.from = "contact@dev.aizpuru.eu";
    Accounts.emailTemplates.siteName = "dev.aizpuru.eu";
  }
}
export {
  MailServer
};