Meteor.publish('users', function() {
    return Meteor.users.find({}, {
        fields: {
            '_id': 1,
            'roles': 1
        }
    });
})

Meteor.publish('images', function imagesPublication(id) {
    if (this.userId) {
        if (id) {
            return Images.find({
                author: id
            });
        }
        return Images.find();
    }
});

Meteor.publish('positions', function positionsPublication() {
    if (this.userId) {
        return Positions.find();
    }
});

Meteor.publish('membersPrivate', function membersPrivatePublication() {
    if (this.userId) {
        return MembersPrivate.find({
            author: this.userId
        });
    }
});

Meteor.publish('members', function membersPublication(id) {
    if (this.userId) {
        if (id) {
            return Members.find({
                author: id
            });
        }
        return Members.find();
    }
});

Meteor.publish('memberspublic', function membersPublicPublication() {
    var transform = function(doc) {
        toKeep = ['_id', 'firstname', 'name', 'diploma', 'description'];
        img = Images.findOne({
            author: doc.author
        });
        Object.keys(doc).forEach(function(itm) {
            if (!_.include(toKeep, itm)) delete doc[itm];
        });
        if (img) {
            doc.image = img.imageurl;
        }
        return doc;
    }

    var self = this;

    var observer = Members.find({
        helper: true
    }).observe({
        added: function(document) {
            self.added('collection_name', document._id, transform(document));
        },
        changed: function(newDocument, oldDocument) {
            self.changed('collection_name', oldDocument._id, transform(newDocument));
        },
        removed: function(oldDocument) {
            self.removed('collection_name', oldDocument._id);
        }
    });

    self.onStop(function() {
        observer.stop();
    });

    self.ready();
});

Internships.allow({
    insert: function(userId, doc) {
        // only allow posting if you are logged in
        return !!userId;
    },
    update: function(userId, doc) {
        // only allow updating if you are logged in
        return !!userId;
    },
    remove: function(userID, doc) {
        //only allow deleting if you are owner
        return doc.submittedById === Meteor.userId();
    }
});
Meteor.publish(null, function internshipsPublication() {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
        return Internships.find();
    } else if (Roles.userIsInRole(this.userId, ['validated'])) {
        return Internships.find({
            $or: [{
                validated: true
            }, {
                author: this.userId
            }]
        });
    }
});
